#!/usr/bin/env python2
#encoding: UTF-8

# Author: Michal Oprendek, michal@oprendek.sk
# All rights reserved.
from itertools import permutations
import sys
from collections import OrderedDict
import json

entrance = "M"

dist = {}
dist["M-10"] = 487.90
dist["M-11"] = 531.74
dist["M-12"] = 920.65
dist["M-1"] = 100.41
dist["M-2"] = 301.23
dist["M-3"] = 581.24
dist["M-4"] = 1002.68
dist["M-5"] = 767.92
dist["M-6"] = 987.12
dist["M-7"] = 777.82
dist["M-8"] = 760.85
dist["M-9"] = 927.72
dist["10-M"] = 487.90
dist["10-11"] = 634.98
dist["10-12"] = 774.99
dist["10-1"] = 387.49
dist["10-2"] = 404.47
dist["10-3"] = 684.48
dist["10-4"] = 514.77
dist["10-5"] = 280.01
dist["10-6"] = 784.89
dist["10-7"] = 289.91
dist["10-8"] = 272.94
dist["10-9"] = 439.82
dist["11-M"] = 531.74
dist["11-10"] = 634.98
dist["11-12"] = 388.91
dist["11-1"] = 431.34
dist["11-2"] = 230.52
dist["11-3"] = 49.50
dist["11-4"] = 649.12
dist["11-5"] = 915.00
dist["11-6"] = 455.38
dist["11-7"] = 924.90
dist["11-8"] = 907.93
dist["11-9"] = 1074.80
dist["12-M"] = 920.65
dist["12-10"] = 774.99
dist["12-11"] = 388.91
dist["12-1"] = 820.24
dist["12-2"] = 619.43
dist["12-3"] = 339.41
dist["12-4"] = 260.22
dist["12-5"] = 585.48
dist["12-6"] = 745.29
dist["12-7"] = 595.38
dist["12-8"] = 578.41
dist["12-9"] = 745.29
dist["1-M"] = 100.41
dist["1-10"] = 387.49
dist["1-11"] = 431.34
dist["1-12"] = 820.24
dist["1-2"] = 200.82
dist["1-3"] = 480.83
dist["1-4"] = 902.27
dist["1-5"] = 667.51
dist["1-6"] = 886.71
dist["1-7"] = 677.41
dist["1-8"] = 660.44
dist["1-9"] = 827.31
dist["2-M"] = 301.23
dist["2-10"] = 404.47
dist["2-11"] = 230.52
dist["2-12"] = 619.43
dist["2-1"] = 200.82
dist["2-3"] = 280.01
dist["2-4"] = 879.64
dist["2-5"] = 684.48
dist["2-6"] = 685.89
dist["2-7"] = 694.38
dist["2-8"] = 677.41
dist["2-9"] = 844.29
dist["3-M"] = 581.24
dist["3-10"] = 684.48
dist["3-11"] = 49.50
dist["3-12"] = 339.41
dist["3-1"] = 480.83
dist["3-2"] = 280.01
dist["3-4"] = 599.63
dist["3-5"] = 910.75
dist["3-6"] = 405.88
dist["3-7"] = 900.85
dist["3-8"] = 917.82
dist["3-9"] = 1084.70
dist["4-M"] = 1002.68
dist["4-10"] = 514.77
dist["4-11"] = 649.12
dist["4-12"] = 260.22
dist["4-1"] = 902.27
dist["4-2"] = 879.64
dist["4-3"] = 599.63
dist["4-5"] = 325.27
dist["4-6"] = 830.14
dist["4-7"] = 335.17
dist["4-8"] = 318.20
dist["4-9"] = 485.08
dist["5-M"] = 767.92
dist["5-10"] = 280.01
dist["5-11"] = 915.00
dist["5-12"] = 585.48
dist["5-1"] = 667.51
dist["5-2"] = 684.48
dist["5-3"] = 910.75
dist["5-4"] = 325.27
dist["5-6"] = 504.87
dist["5-7"] = 9.90
dist["5-8"] = 7.07
dist["5-9"] = 182.43
dist["6-M"] = 987.12
dist["6-10"] = 784.89
dist["6-11"] = 455.38
dist["6-12"] = 745.29
dist["6-1"] = 886.71
dist["6-2"] = 685.89
dist["6-3"] = 405.88
dist["6-4"] = 830.14
dist["6-5"] = 504.87
dist["6-7"] = 494.97
dist["6-8"] = 511.95
dist["6-9"] = 687.31
dist["7-M"] = 777.82
dist["7-10"] = 289.91
dist["7-11"] = 924.90
dist["7-12"] = 595.38
dist["7-1"] = 677.41
dist["7-2"] = 694.38
dist["7-3"] = 900.85
dist["7-4"] = 335.17
dist["7-5"] = 9.90
dist["7-6"] = 494.97
dist["7-8"] = 16.97
dist["7-9"] = 192.33
dist["8-M"] = 760.85
dist["8-10"] = 272.94
dist["8-11"] = 907.93
dist["8-12"] = 578.41
dist["8-1"] = 660.44
dist["8-2"] = 677.41
dist["8-3"] = 917.82
dist["8-4"] = 318.20
dist["8-5"] = 7.07
dist["8-6"] = 511.95
dist["8-7"] = 16.97
dist["8-9"] = 175.36
dist["9-M"] = 927.72
dist["9-10"] = 439.82
dist["9-11"] = 1074.80
dist["9-12"] = 745.29
dist["9-1"] = 827.31
dist["9-2"] = 844.29
dist["9-3"] = 1084.70
dist["9-4"] = 485.08
dist["9-5"] = 182.43
dist["9-6"] = 687.31
dist["9-7"] = 192.33
dist["9-8"] = 175.36

def measure(permutation, verbose=False):
    if verbose:
        print permutation
    last = entrance
    length = 0
    distances = OrderedDict()
    for point in permutation:
        pointToPointDistance = dist[last + '-' + point]
        distances[point] = pointToPointDistance
        length += pointToPointDistance
        if verbose:
            print last + '-' + point, pointToPointDistance
        last = point
    pointToPointDistance = dist[last + '-' + entrance]
    if verbose:
        print last + '-' + entrance, pointToPointDistance
    length += pointToPointDistance;
    distances[entrance] = pointToPointDistance
    return length, distances

def find_optimal_order(items):
    minLength = sys.float_info.max
    minPath = None
    for permutation in permutations(items, len(items)):
        length, distances = measure(permutation)
        if length < minLength:
            minLength = length
            minPath = distances
        #print permutation, length
    
    return minPath

if __name__ == "__main__":
    #items = {"1", "2", "3"}
    items = {"3", "5", "6", "8"}
    #items = map(lambda x: str(x), range(1, 10))
    print items
    optimalOrder = find_optimal_order(items)
    print "Winner is:"
    print(json.dumps(optimalOrder, indent=4))
    
    
    

        

            