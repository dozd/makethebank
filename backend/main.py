#!/usr/bin/env python2

from flask import Flask
from flask import request
from flask_restful import Api
from flask_restful import Resource
import multihop_path
import traveling_salesman

app = Flask(__name__)
api = Api(app)

table = {
    '1':    'Perfumes',
    '2':    'Perfumes',
    '3':    'Refrigerators',
    '4':    'Refrigerators',
    '5':    'Shoes',
    '6':    'Shoes',
    '7':    'Suits',
    '8':    'Suits',
    '9':    'Bikes',
    '10':   'Bikes',
    '11':   'Watches',
    '12':   'Watches',
    'M':    'exit'
}

offers = {
    'Perfumes': [
        {
            'id':'1',
            'name':'Yves Saint Laurent Babydoll Gift Set',
            'price': 25.00
        },
        {
            'id':'2',
            'name':'Little Mix Gold Magic 50ml Eau de Parfum Gift Set',
            'price': 26.95
        }
    ],
    'Refrigerators': [
        {
            'id':'3',
            'name':'LG GSL545PVYV American Style Fridge Freezer, Silver',
            'price': 999.00
        },
        {
            'id':'4',
            'name':'Samsung RF56J9040SR 4-Door American Style Fridge ',
            'price': 2699.00
        }
    ],
    'Shoes': [
        {
            'id':'5',
            'name':'GEOX Sneakers Symbol',
            'price': 99.00

        },
        {
            'id':'6',
            'name':'CLARKS Hamble Oak',
            'price': 89.00
        }
    ],
    'Suits': [
        {
            'id':'7',
            'name':'TED 2346IU78 Baker Tailored Blue',
            'price': 189.00
        },
        {
            'id':'8',
            'name':'TOPMAN 87J08945 new fit grey skinny fit',
            'price': 179.00
        }
    ],
    'Bikes': [
        {
            'id':'9',
            'name':'KTM 450 SX-F Dirt Bike Plastic',
            'price': 39.89
        },
        {
            'id':'10',
            'name':'Yamaha YZF 450 Quad Bike Plastic',
            'price': 17.89
        }
    ],
    'Watches': [
        {
            'id':'11',
            'name':'BREATHING Navitimer 50th anniversary',
            'price':3788.89
        },
        {
            'id':'12',
            'name':'PIAGET Dancer Watch / 8 4318K',
            'price':3759.99
        }
    ]
}

table_reverse = dict(zip(table.values(), table.keys()))


class Path(Resource):
    def post(self):
        items = request.get_json()

        numbers = [str(table_reverse[item]) for item in items]
        ret = traveling_salesman.find_optimal_order(numbers)
        #remove C from the list
        del ret['M']

        ret_str = []
        for item in ret.keys():
            try:
                ret_str.append(table[item])
            except KeyError:
                pass

        return ret_str

def add_name(dict):
    if 'item' in dict:
        dict['name'] = table[dict['item']]
    return dict

def add_offer_name(dict):
    if 'item' in dict:
        for category, offerList in offers.iteritems():
            for offer in offerList:
               # print dict['item'], offer['id']
                if dict['item'] == offer['id']:
                    dict['name'] = category
    return dict
    

class FullPath(Resource):
    def post(self):
        items = request.get_json()
        ids = [str(table_reverse[item]) for item in items]

        ids.append('M')
        path = multihop_path.get_full_path(ids)
        return map(add_name, path)

class PathToOffer(Resource):
    def post(self):
        item = request.get_json()
        items = []
        items.append(item)
        path = multihop_path.get_full_path(items)
  #      return path
        return map(add_offer_name, path)
    
class Offers(Resource):
    def post(self):
        items = request.get_json()
        ret_offers = []
        i = 1
        for item in items:
            ret_item = {}
            ret_item['name'] = item
            ret_item['offer'] = False
            ret_offers.append(ret_item)
            for offer in offers[item]:
                ret_item = {}
                ret_item['name'] = offer['name']
                ret_item['price'] = offer['price']
                ret_item['mapTarget'] = offer['id']
                ret_item['offer'] = True
                ret_offers.append(ret_item)

        return ret_offers

api.add_resource(Path, '/path')
api.add_resource(FullPath, '/fullPath')
api.add_resource(PathToOffer, '/pathToOffer')
api.add_resource(Offers, '/offers')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
