package dozd.eu.optipath.call;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import dozd.eu.optipath.Backend;
import dozd.eu.optipath.model.Offer;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class GetOffersAsync extends AsyncTask<List<String>, Void, List<Offer>> {
    private final Context context;

    public GetOffersAsync(Context context) {
        this.context = context;
    }

    protected List<Offer> doInBackground(List<String>... items) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.65.29:5000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Backend backend = retrofit.create(Backend.class);
        List<Offer> response;
        try {
             response = backend.getOffers(items[0]).execute().body();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    protected void onPostExecute(Void feed) {
        // TODO: check this.exception

    }
}
