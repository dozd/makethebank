package dozd.eu.optipath.result;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import dozd.eu.optipath.R;
import dozd.eu.optipath.obp.OBPRestClient;

public class ResultActivity extends AppCompatActivity {
    public String[] dataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        dataset = getIntent().getStringArrayExtra("list");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        viewPager.setAdapter(new SectionPagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
//
//        Button pay = (Button) findViewById(R.id.pay);
//        pay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Request the banks available from the API
//                new AsyncTask<Void, Void, String>() {
//
//                    @Override
//                    /**
//                     * @return A String containing the json representing the available banks, or an error message
//                     */
//                    protected String doInBackground(Void... params) {
//                        try {
//                            JSONObject banksJson = OBPRestClient.getBanksJson();
//                            return banksJson.toString();
//                        } catch (ExpiredAccessTokenException e) {
//                            // login again / re-authenticate
//                            redoOAuth();
//                            return "";
//                        } catch (ObpApiCallFailedException e) {
//                            return "Sorry, there was an error!";
//                        }
//                    }
//
//                    @Override
//                    protected void onPostExecute(String result) {
//                        Toast.makeText(ResultActivity.this, "Ahoj", Toast.LENGTH_LONG).show();
//                    }
//                }.execute();
//            }
//        });
    }

    private void redoOAuth() {
        OBPRestClient.clearAccessToken(this);
        Intent oauthActivity = new Intent(this, OAuthActivity.class);
        startActivity(oauthActivity);
    }

    public class SectionPagerAdapter extends FragmentPagerAdapter {

        public SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ResultFragment();
                case 1:
                default:
                    return new MapFragment();

            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Shop list";
                case 1:
                default:
                    return "Map";
            }
        }
    }
}
