package dozd.eu.optipath;

import java.util.List;

import dozd.eu.optipath.model.Offer;
import dozd.eu.optipath.model.PathPoint;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

public interface Backend {

    @POST("/path")
    Call<List<String>> findPath(@Body List<String> items);

    @POST("/fullPath")
    Call<List<PathPoint>> getFullPath(@Body List<String> items);

    @POST("/pathToOffer")
    Call<List<PathPoint>> pathToOffer(@Body String id);

    @POST("/offers")
    Call<List<Offer>> getOffers(@Body List<String> items);

}
