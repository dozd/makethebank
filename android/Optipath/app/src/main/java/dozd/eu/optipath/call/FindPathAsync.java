package dozd.eu.optipath.call;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import dozd.eu.optipath.Backend;
import dozd.eu.optipath.result.ResultActivity;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class FindPathAsync extends AsyncTask<List<String>, Void, Void> {
    private final Context context;

    public FindPathAsync(Context context) {
        this.context = context;
    }

    protected Void doInBackground(List<String>... items) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.65.29:5000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Backend backend = retrofit.create(Backend.class);
        try {
            List<String> response = backend.findPath(items[0]).execute().body();
            Intent intent = new Intent(context, ResultActivity.class);
            intent.putExtra("list", response.toArray(new String[response.size()]));
            context.startActivity(intent);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(Void feed) {
        // TODO: check this.exception

    }
}
