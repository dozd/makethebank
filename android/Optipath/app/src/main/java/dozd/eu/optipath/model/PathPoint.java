package dozd.eu.optipath.model;

public class PathPoint {
    private int x;
    private int y;
    private String item;
    private String name;
    private boolean stop;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return 1560 - y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
