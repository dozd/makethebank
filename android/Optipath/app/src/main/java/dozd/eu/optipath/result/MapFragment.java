package dozd.eu.optipath.result;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import dozd.eu.optipath.R;
import dozd.eu.optipath.call.GetFullPathAsync;
import dozd.eu.optipath.model.PathPoint;

public class MapFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);
        MyImageView lines = (MyImageView) view.findViewById(R.id.lineView);

        List<PathPoint> pathPoints;
        try {
            pathPoints = new GetFullPathAsync(getActivity()).execute(Arrays.asList(((ResultActivity) getActivity()).dataset)).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        lines.setPathPoints(pathPoints);

        return view;
    }


}