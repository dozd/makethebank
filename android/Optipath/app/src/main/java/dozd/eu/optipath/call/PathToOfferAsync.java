package dozd.eu.optipath.call;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import dozd.eu.optipath.Backend;
import dozd.eu.optipath.model.PathPoint;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class PathToOfferAsync extends AsyncTask<String, Void, List<PathPoint>> {
    private final Context context;

    public PathToOfferAsync(Context context) {
        this.context = context;
    }

    protected List<PathPoint> doInBackground(String... items) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.65.29:5000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Backend backend = retrofit.create(Backend.class);
        List<PathPoint> response;
        try {
             response = backend.pathToOffer(items[0]).execute().body();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    protected void onPostExecute(Void feed) {
        // TODO: check this.exception

    }
}
