package dozd.eu.optipath.result;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.util.List;

import dozd.eu.optipath.model.PathPoint;

public class MyImageView extends ImageView {
    private static final Paint paint = new Paint();
    private static final Paint stopPaint = new Paint();
    private static final Paint textPaint = new Paint();

    static {
        paint.setColor(Color.rgb(178, 3, 3));
        paint.setStrokeWidth(12);
        stopPaint.setColor(Color.rgb(255, 222, 0));
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(45);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
    }

    private List<PathPoint> pathPoints;

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (pathPoints == null) {
            return;
        }
        int curX = 0, curY = 0;
        for (PathPoint point : pathPoints) {
            if (curX == 0 || curY == 0) {
                curX = point.getX();
                curY = point.getY();
                continue;
            }
            canvas.drawLine(curX, curY, point.getX(), point.getY(), paint);
            canvas.drawCircle(point.getX(), point.getY(), 6, paint);
            curX = point.getX();
            curY = point.getY();
        }

        for (PathPoint point : pathPoints) {
            if (point.isStop()) {
                canvas.drawCircle(point.getX(), point.getY(), 20, stopPaint);
            }
        }

        for (PathPoint point : pathPoints) {
            if (point.isStop() && point.getName() != null) {
                canvas.drawText(point.getName(), point.getX() + 20, point.getY() - 12, textPaint);
            }
        }
    }

    public void setPathPoints(List<PathPoint> pathPoints) {
        this.pathPoints = pathPoints;
    }
}
