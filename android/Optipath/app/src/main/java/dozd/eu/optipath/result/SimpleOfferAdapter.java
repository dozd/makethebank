package dozd.eu.optipath.result;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import dozd.eu.optipath.R;
import dozd.eu.optipath.model.Offer;

public class SimpleOfferAdapter extends RecyclerView.Adapter<SimpleOfferAdapter.SimpleViewHolder> {

    private final Context mContext;
    private List<Offer> mData;

    /*
    public void add(String s, int position) {
        position = position == -1 ? getItemCount() : position;
        mData.add(position, s);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        if (position < getItemCount()) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }*/

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final Button button;
        public final TextView price;

        public SimpleViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            button = (Button) view.findViewById(R.id.payButton);
            price = (TextView) view.findViewById(R.id.price);
        }
    }

    public SimpleOfferAdapter(Context context, List<Offer> data) {
        mContext = context;
        mData = data;
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.simple_offer_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final Offer item = mData.get(position);
        holder.title.setText(item.getName());
        if (item.isOffer()) {
            holder.button.setVisibility(View.VISIBLE);
            if (item.getPrice() != null) {
                holder.price.setText(item.getPrice().toString() + " EUR");
            }
            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, PayedActivity.class);
                    intent.putExtra("item", item.getMapTarget());
                    mContext.startActivity(intent);
                }
            });
        } else {
            holder.title.setTextSize(18);
            holder.title.setTypeface(null, Typeface.BOLD);
            holder.price.setVisibility(View.INVISIBLE);
            holder.button.setVisibility(View.INVISIBLE);
        }

//        holder.checkbox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (holder.checkbox.isChecked()) {
//                    if (!checked.contains(item)) {
//                        checked.add(item);
//                    }
//                } else {
//                    checked.remove(item);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}