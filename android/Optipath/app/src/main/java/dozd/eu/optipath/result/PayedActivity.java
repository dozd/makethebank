package dozd.eu.optipath.result;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import dozd.eu.optipath.R;
import dozd.eu.optipath.call.PathToOfferAsync;
import dozd.eu.optipath.model.PathPoint;

public class PayedActivity extends AppCompatActivity {
    String item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payed);

        item = getIntent().getStringExtra("item");

        final Button id = (Button) findViewById(R.id.id);
        id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id.setText("Paying ...");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        id.setText("Paid ID: " + UUID.randomUUID().toString().substring(0, 5));
                    }
                }, 1500);

            }
        });
        MyImageView lines = (MyImageView) findViewById(R.id.lineView);

        List<PathPoint> pathPoints;
        try {
            pathPoints = new PathToOfferAsync(this).execute(item).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        lines.setPathPoints(pathPoints);
    }
}
