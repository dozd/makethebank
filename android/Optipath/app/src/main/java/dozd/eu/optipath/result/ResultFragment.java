package dozd.eu.optipath.result;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import dozd.eu.optipath.R;
import dozd.eu.optipath.call.GetOffersAsync;
import dozd.eu.optipath.model.Offer;

public class ResultFragment extends Fragment {

    private SimpleOfferAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.result_fragment, container, false);
        List<Offer> offers;
        try {
            offers = new GetOffersAsync(getActivity()).execute(Arrays.asList(((ResultActivity) getActivity()).dataset)).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        RecyclerView list = (RecyclerView) view.findViewById(R.id.shopList);
        adapter = new SimpleOfferAdapter(getActivity(), offers);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter);

        return view;

    }
}