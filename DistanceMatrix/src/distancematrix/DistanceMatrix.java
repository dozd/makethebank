/*
 * Author: Michal Oprendek, michal@oprendek.sk
 * All rights reserved.
 */
package distancematrix;

import static distancematrix.Dijkstra.computePaths;
import static distancematrix.Dijkstra.getShortestPathTo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Miso
 */
public class DistanceMatrix {

    /**
     * @param args the command line arguments
     */
    private static class Point {

        final String name;
        final int x;
        final int y;
        final Vertex vertex;

        public Point(String name, int x, int y) {
            this.name = name;
            this.x = x;
            this.y = y;
            this.vertex = new Vertex(name);
        }
    }

    private static Map<String, Point> points = new HashMap<>();

    private static void addPoint(String name, int x, int y) {
        points.put(name, new Point(name, y, 1560 - x));
    }

    private static double sq(double x) {
        return x * x;
    }

    private static void addBidirectionalLink(String point1, String point2) {
        Point p1 = points.get(point1);
        Point p2 = points.get(point2);
        if (p1 == null) {
            throw new IllegalArgumentException("point1 is unknown");
        }
        if (p2 == null) {
            throw new IllegalArgumentException("point2 is unknown");
        }

        double distance = Math.sqrt((sq(p1.x - p2.x) + sq(p1.y - p2.y)));
        p1.vertex.adjacencies.add(new Edge(p2.vertex, distance));
        p2.vertex.adjacencies.add(new Edge(p1.vertex, distance));
    }

    public static void main(String[] args) {

        addPoint("A", 23, 717);
        addPoint("B", 268, 812);
        addPoint("C", 551, 840);
        addPoint("D", 809, 803);
        addPoint("2", 84, 656);
        addPoint("3", 658, 826);
        addPoint("4", 753, 402);
        addPoint("L", 150, 579);
        addPoint("M", 23, 443);
        addPoint("1", 84, 514);
        addPoint("K", 250, 500);
        addPoint("10", 389, 370);
        addPoint("11", 358, 819);
        addPoint("9", 457, 59);
        addPoint("J", 554, 204);
        addPoint("I", 581, 180);
        addPoint("8", 713, 177);
        addPoint("E", 1048, 698);
        addPoint("12", 938, 586);
        addPoint("F", 1309, 512);
        addPoint("6", 1271, 539);
        addPoint("G", 1394, 412);
        addPoint("H", 1511, 191);
        addPoint("7", 1303, 189);
        addPoint("5", 1001, 182);

        addBidirectionalLink("A", "B");
        addBidirectionalLink("A", "2");
        addBidirectionalLink("B", "11");
        addBidirectionalLink("B", "K");
        addBidirectionalLink("11", "C");
        addBidirectionalLink("C", "3");
        addBidirectionalLink("3", "D");
        addBidirectionalLink("D", "E");
        addBidirectionalLink("E", "6");
        addBidirectionalLink("3", "12");
        addBidirectionalLink("6", "F");
        addBidirectionalLink("F", "G");
        addBidirectionalLink("G", "H");
        addBidirectionalLink("H", "7");
        addBidirectionalLink("7", "5");
        addBidirectionalLink("5", "8");
        addBidirectionalLink("8", "I");
        addBidirectionalLink("I", "J");
        addBidirectionalLink("I", "9");
        addBidirectionalLink("J", "10");
        addBidirectionalLink("J", "4");
        addBidirectionalLink("4", "12");
        addBidirectionalLink("10", "K");
        addBidirectionalLink("K", "L");
        addBidirectionalLink("L", "2");
        addBidirectionalLink("L", "1");
        addBidirectionalLink("1", "M");

        for (Point src : points.values()) {
            for (Point dest : points.values()) {
                dest.vertex.minDistance = Double.POSITIVE_INFINITY;;
                dest.vertex.previous = null;
            }
            computePaths(src.vertex);
            for (Point dest : points.values()) {
                Vertex v = dest.vertex;
                if ((isRelevant(src) && isRelevant(dest)) && src != dest) {
//                System.out.printf("%10s %10s %10.2f%n", src.name, dest.name, v.minDistance);
                    System.out.printf("dist[\"%s-%s\"] = %.2f%n", src.name, dest.name, v.minDistance);
                }
            }
        }
        for (Point src : points.values()) {
            for (Point dest : points.values()) {
                dest.vertex.minDistance = Double.POSITIVE_INFINITY;;
                dest.vertex.previous = null;
            }
            computePaths(src.vertex);
            for (Point dest : points.values()) {
                Vertex v = dest.vertex;
                if ((isRelevant(src) && isRelevant(dest)) && src != dest) {
                    StringBuilder sb = new StringBuilder("[");
                    List<Vertex> path = getShortestPathTo(v);
                    for (Vertex vertex : path) {
                        sb.append("(");
                        Point p = points.get(vertex.name);
                        sb.append(p.x);
                        sb.append(", ");
                        sb.append(p.y);
                        sb.append("), ");
                    }
                    sb.append("]");
                    sb.replace(sb.length() - 3, sb.length() - 1, "");
                    System.out.printf("path[\"%s-%s\"] = %s%n", src.name, dest.name, sb.toString());
                }
            }
        }
    }

    private static boolean isRelevant(Point p) {
        return p.name.matches("[0-9]*") || "M".equals(p.name);
    }

}
